ARG VERSION=latest

FROM bitnami/kubectl:${VERSION}

COPY install.sh /root/
USER root
RUN /bin/bash /root/install.sh
USER kubectl
WORKDIR /home/kubectl
ENTRYPOINT []
CMD ["/bin/bash"]
