#!/bin/bash
# Stop on error
set -e
# Install mandatory programs and utilities
export DEBIAN_FRONTEND=noninteractive
apt-get --yes --quiet=2 update
apt-get --yes --quiet=2 install \
	curl \
	ca-certificates \
	git \
	rsync
apt-get clean
rm -rf /var/lib/apt/lists/*
curl --silent https://api.github.com/repos/kubernetes-sigs/kustomize/releases/latest |\
  grep browser_download |\
  grep linux |\
  cut -d '"' -f 4 |\
  xargs curl --silent --location -o /usr/local/bin/kustomize
chmod +x /usr/local/bin/kustomize
adduser --disabled-password --gecos "kubectl user,,," --home /home/kubectl --shell /bin/bash --uid 1001 kubectl
